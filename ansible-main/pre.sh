#!/bin/bash

sudo apt-get install python3-venv &&\
python3 -m venv --system-site-packages ansible_env &&\
source ./ansible_env/bin/activate &&\
pip install ansible
